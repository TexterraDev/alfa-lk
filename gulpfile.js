var gulp = require('gulp'),
    sass = require('gulp-sass'),
    autoprefixer = require('gulp-autoprefixer'),
    concat = require('gulp-concat'),
    uglify = require('gulp-uglify'),
    cssnano = require('gulp-cssnano'),
    rename = require('gulp-rename'),
    browserSync = require('browser-sync')

gulp.task('sass', function(){
    return gulp.src(['dist/sass/main.scss'])
        .pipe(sass({
          includePaths: require('node-normalize-scss').includePaths
        }))
        .pipe(autoprefixer('last 2 versions'))
        .pipe(concat('styles.min.css'))
        .pipe(cssnano())
        .pipe(gulp.dest('dist/css'))
        .pipe(browserSync.reload({stream: true}))
});

gulp.task('scripts-libs', function(){
  return gulp.src([
    'node_modules/jquery/dist/jquery.js',
    'node_modules/@fancyapps/fancybox/dist/jquery.fancybox.min.js'
  ])
  .pipe(concat('libs.min.js'))
  .pipe(uglify())
  .pipe(gulp.dest('dist/js'));
});

gulp.task('css-libs', ['sass'], function(){
  return gulp.src([
    'node_modules/@fancyapps/fancybox/dist/jquery.fancybox.min.css'
  ])
  .pipe(concat('libs.css'))
  .pipe(cssnano())
  .pipe(rename({suffix: '.min'}))
  .pipe(gulp.dest('dist/css'));
});


gulp.task('browser-sync', function(){
    browserSync({
      server: {
        baseDir: 'dist',
        index: "index.html"
      },
      notify: false
    });
});

gulp.task('watch', ['browser-sync', 'css-libs', 'scripts-libs'], function(){
    gulp.watch('dist/sass/**/*.scss', ['sass']);
    gulp.watch('dist/*.html', browserSync.reload);
    gulp.watch('dist/js/**/*.js', browserSync.reload);
});
